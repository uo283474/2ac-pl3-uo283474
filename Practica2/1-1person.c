#include <stdio.h>
#include <string.h>

struct _Person
{
    char name[30];
    int heightcm;
    double weightkg;
};
// This abbreviates the type name
typedef struct _Person Person;
// Type name abbreviations
typedef Person* PPerson;

int main(int argc, char* argv[])
{
    Person Peter;
    strcpy(Peter.name, "Peter");
    Peter.heightcm = 175.0;

    // TODO: Assign the weight
    Peter.weightkg = 78.7;
    printf("Peter's height: %i  cm Peter's weight:%.2f kg ", Peter.heightcm,Peter.weightkg);	
    // TODO: Show the information of the Peter data structure on the screen
    Person Javier;
    PPerson pJavier;

    // Memory location of variable Javier is assigned to the pointer
    //pJavier = &Javier;
    Javier.heightcm = 180;
    Javier.weightkg = 84.0;
    //pJavier->weightkg = 83.2;
    printf("Javierr's height: %i  cm Javier's weight:%.2f kg ", Javier.heightcm,Javier.weightkg);


    return 0;
}
