#include <stdio.h>
#include <stdlib.h>

int copy(char * cadena, char * destination, unsigned int size){
    for (int i = 0; i < size; i++){
        destination[i] = cadena[i];
    }
    return 0;
}

int main(){
    char cadena[] = "cadena de prueba a copiar";
    char * destination;
    unsigned int size = 100;
    destination=(char *)malloc(size * sizeof(char));
    copy(cadena, destination, size);

    printf("Destination: %s \n", destination );
}
