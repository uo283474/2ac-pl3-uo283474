#include <stdio.h>

int add(int x[], int num){
    int sum = 0;
    for (int i=0; i<num;i++){
        sum=sum+x[i];
}
return sum;
}

#define NUM_ELEMENTS 7

int main()
{
    int vector[NUM_ELEMENTS] = { 2, 5, -2, 9, 12, -4, 3 };
    int result;

    result = add(vector, NUM_ELEMENTS);
    printf("The addition is: %d\n", result);
    return 0;
}
